const { useBase } = require('./base');
const { options } = require('./claOptions');

const insert = async () => {
  try {
    const { base } = await useBase();

    await base('newsPosts').insert({
      title: options.title,
      text: options.text
    })

    console.log('insert success')
  } catch (err) {
    console.log(err)
  }
}

(async () => {
  await insert()
})()