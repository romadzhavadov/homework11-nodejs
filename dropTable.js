const knex = require('knex');
const { useBase } = require('./base');

const dropTable = async () => {
  const { base } = await useBase();

  await base.schema.dropTableIfExists('newsPosts');

  console.log("Table 'newsPosts' dropped");
}

dropTable();