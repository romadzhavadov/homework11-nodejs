const { useBase } = require('./base');

const getAll = async () => {
  try {
    const { base } = await useBase();

    const allPosts = await base('newsPosts').select('*');
    console.log(allPosts);
  } catch (err) {
    console.error('Error:', err);
  }
};


(async () => {
  await getAll()
})()