const commandLineArgs = require('command-line-args');
const { useBase } = require('./base');
const { options } = require('./claOptions');

const updatePost = async () => {
  try {
    const { base } = await useBase();

    if (!options.id) {
      console.log('Please provide an id.');
      return;
    }

    if (!options.title && !options.text) {
      console.log('Please provide data to update.');
      return;
    }

    const updatedData = {};
    if (options.title) {
      updatedData.title = options.title;
      return
    }

    if (options.text) {
      updatedData.text = options.text;
      return
    }

    await base('newsPosts').where('id', options.id).update(updatedData);

    console.log('Post updated successfully.');

  } catch (err) {
    console.error('Error:', err);
  }
};

(async () => {
  await updatePost()
})()