const { useBase } = require('./base');

const createTable = async () => {
  const { base } = await useBase();
  return new Promise(async (resolve, reject) => {
    await base.schema.createTable('newsPosts ', table => {
      table.increments('id').primary();
      table.string('title').notNullable();
      table.text('text').notNullable();
      table.dateTime('created_at').defaultTo(base.fn.now());
      table.dateTime('updated_at').defaultTo(base.fn.now());
      resolve("Table created")
    }).catch(err => {
      reject(err)
    })
  })
}

(async () => {
  console.log(await createTable())
})()