const { useBase } = require('./base');
const { options } = require('./claOptions');

const deletePost = async () => {
  try {
    const { base } = await useBase();

    await base('newsPosts').where('id', options.id).del();
    console.log('Record deleted successfully.');

  } catch (err) {
    console.error('Error:', err);
  }
};

(async () => {
  await deletePost();
})();
