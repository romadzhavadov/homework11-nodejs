const commandLineArgs = require('command-line-args');

const optionDefinitions = [
  { name: 'title', type: String },
  { name: 'text', type: String },
  { name: 'id', type: Number },
];

const options = commandLineArgs(optionDefinitions);

module.exports = {
  options
};