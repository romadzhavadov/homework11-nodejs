const { useBase } = require('./base');
const { options } = require('./claOptions');

const getById = async () => {
  try {
    const { base } = await useBase();

    if (options.id) {
      const post = await base('newsPosts').where('id', options.id).first();
      console.log(post);
    } else {
      console.log('Please provide an id.');
    }

  } catch (err) {
    console.error('Error:', err);
  }
};

(async () => {
  await getById()
})()